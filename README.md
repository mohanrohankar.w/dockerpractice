# DockerPractice

Let's explore and learn more about Docker !!

## Getting started

Docker is an operating system virtualization technology that allows applications to be packaged as containers.

IMP Refrence Links :

- [ ] [Docker Hub](https://hub.docker.com/search?type=image)
- [ ] [Docker Help](https://docs.docker.com/engine/reference/commandline/run/)

## Docker Commands

Run Jenkins in Docker Container

- [ ] [Docker - Jenkins Command Help](https://www.jenkins.io/doc/book/installing/docker/#downloading-and-running-jenkins-in-docker)

```
docker pull jenkins/jenkins
docker images
docker run --name myjenkins -p 8080:8080 -p 50000:50000 -v //c/jenkins_home:/var/jenkins_home jenkins/jenkins
docker ps -a
docker stop container_name/id
dcoker restart container_name/id
docker rm container_name/id
docker rmi image_name/id
docker exec -it container_name/id bash
exit
```

